#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdbool.h>
#include <string.h>
#include "csapp.h"
#include "uECC.h"
#include "sha256.h"
#include "aes.h"
#include <strings.h>

void printHelp(void);
void printError(char);
void printHex(uint8_t *buf, int len, char *startmessage);
void downloadfile(int clientfd, uint8_t aeskey[32], uint8_t iv[16], char *filename);
void uploadfile(int clientfd, uint8_t aeskey[32], uint8_t iv[16], char *filename);
void downloadfilelist(int clientfd,uint8_t aeskey[32], uint8_t iv[16]);
void downloadfilehash(int clientfd, uint8_t aeskey[32], uint8_t iv[16], char *filename);
void computefilehash(char *filename);
void checkfile(int clientfd, uint8_t aeskey[32], uint8_t iv[16], char *filename);
void pad(char *buffer, int filled, int total);

int main(int argc, char *argv[]){
	extern char *optarg;
	extern int optind;
	int option;
	while((option=getopt(argc,argv,"h"))!=-1) {
		switch(option) {
		case 'h':
			printHelp();     /* will never return */
			break;
		default:
			exit(0);
			break;
		}
	}

	char serverip[64];
	char serverport[6];
	if(optind!=argc-2) { /* optind is done, there are NOT 2 arguments left (server and port), use defaults */
		printf("Arguments not complete. Using default settings\n");
		strcpy(serverip,"127.0.0.1");
		strcpy(serverport,"8080");
	} else {
		strcpy(serverip,argv[optind]);
		strcpy(serverport,argv[optind+1]);
	}

	printf("Connecting to %s:%s\n",serverip,serverport);
	int clientfd=Open_clientfd(serverip,serverport);
	printf("Connection established. Exchanging keys...\n");
	rio_writen(clientfd,"START\n",6);

	/* Begin Diffie-Hellman key exchange process */
	uint8_t private[32] = {0};
	uint8_t public[64] = {0};
	uint8_t serverpublic[64] = {0};
	uint8_t secret[32] = {0};
	const struct uECC_Curve_t *curve=uECC_secp160r1();

	uECC_make_key(public, private, curve);
	rio_readn(clientfd,serverpublic,64);
	rio_writen(clientfd,public,64);

	uECC_shared_secret(serverpublic, private, secret, curve);
	/* secret now holds a shared Diffie-Hellman key that must be hashed */

	/* SHA-256 hashing of secret key */
	uint8_t aeskey[32];
	SHA256_CTX ctx;
	sha256_init(&ctx);
	sha256_update(&ctx, secret, 32);
	sha256_final(&ctx, aeskey);
	printf("Keys exchanged. Reading IV\n");
	/* aeskey now holds an AES symmetric key */
	/* clear secrets from memory */
    memset(private,0,32);
    memset(secret,0,32);
	/*explicit_bzero(private,32);
	explicit_bzero(secret,32);*/

	/* get random IV from server */
	uint8_t iv[16];
	rio_readn(clientfd,iv,16);
	printf("IV received. Crypto ready\n");

	/* start main client loop */
	rio_t rp;
	rio_readinitb(&rp, STDIN_FILENO);
	char command[64];
	int n;
	printf("> ");
	fflush(stdout);
	while((n=rio_readlineb(&rp,command,64))>4 || strncmp(command,"BYE\n",4)!=0) { /* User didn't input BYE\n */
		command[n-1]='\0'; /* Remove newline from end of line */
		switch(command[0]) {
		case 'G': /* GET */
		case 'g':
			/* call downloadfile, pass a pointer to the 4th char of command (first 3 must be GET and a space) */
			downloadfile(clientfd,aeskey,iv, command+4);
			break;
		case 'P': /* PUT */
		case 'p':
			/* call uploadfile, pass a pointer to the 4th char of command (first 3 must be PUT and a space) */
			uploadfile(clientfd,aeskey,iv,command+4);
			break;
		case 'L': /* LIST */
		case 'l':
			/* call downloadfilelist */
			downloadfilelist(clientfd,aeskey,iv);
			break;
		case 'C': /* CHECK */
		case 'c':
			/* call checkfile, pass a pointer to the 7th char of command (first 6 must be CHECK and a space) */
			checkfile(clientfd, aeskey, iv, command+6);
			break;
		case 'H': /* HASH or HASHLOCAL */
		case 'h':
			if(command[4]==' ') { /* 5th char is space, command was HASH <filename> */
				/* call downloadfilehash, pass a pointer to the 6th char of command (first 4 must be HASH and a space) */
				downloadfilehash(clientfd,aeskey,iv,command+5);
				break;
			} else if(command[4]=='L'||command[4]=='l') { /* 5th char is L, command was HASHLOCAL <filename> */
				/* call computefilehash, pass a pointer to 11th char of command (first 10 were HASHLOCAL and a space) */
				computefilehash(command+10);
				break;
			}
		default:
			fprintf(stderr,"Command %s not recognized!\n",command);
			break;
		}
		printf("> ");
		fflush(stdout);
	}
}

void printHelp(){
	printf("Uso: file_client [opciones] [IP servidor] [puerto]\n");
	printf("Opciones:\n");
	printf("    -h: ayuda (este mensaje)\n");
	exit(0);
}

void printError(char option){
	fprintf(stderr,"Unrecognized option %c!\n",option);
	printHelp(); /* will never return */
}

void printHex(uint8_t *buf, int len, char *startmessage){
	printf("%s",startmessage);
	for(int i=0; i<len; i++)
		printf("%02x",buf[i]);
	printf("\n");
}

void downloadfile(int clientfd, uint8_t aeskey[32], uint8_t iv[16], char *filename){
	char *command=(char *)calloc(64,sizeof(char));
	sprintf(command,"GET %s",filename);

	WORD key_schedule[60];
	BYTE enc_buf[64];
	BYTE plaintext[64];
	pad(command,strlen(command),64);
	memcpy(plaintext,command,64);

	aes_key_setup(aeskey, key_schedule, 256);
	aes_encrypt_cbc(plaintext, 64, enc_buf, key_schedule, 256, iv);
	rio_writen(clientfd,enc_buf,64);

	char filesize[64];
	BYTE dec_buf[64];
	int realfilesize;
	rio_readn(clientfd,filesize,64);
	aes_decrypt_cbc(filesize,64,dec_buf,key_schedule,256,iv);
	sscanf(dec_buf,"%d",&realfilesize);
	/* realfilesize now holds the size of the requested file, or 0 if not found */

	if(realfilesize==0) {
		fprintf(stderr,"ERROR: Server couldn't find file %s\n",filename);
		return;
	}

	int localfile=open(filename,O_WRONLY|O_CREAT|O_TRUNC,0777);
	char buf[64];
	int downloaded=0;
	while(downloaded<realfilesize&&rio_readn(clientfd,buf,64)>0) {
		aes_decrypt_cbc(buf,64,dec_buf,key_schedule,256,iv);
		downloaded+=rio_writen(localfile,dec_buf,(realfilesize-downloaded)>=64 ? 64 : (realfilesize-downloaded));
	}
	close(localfile);
	printf("OK. %d bytes downloaded\n",downloaded);
}

void uploadfile(int clientfd, uint8_t aeskey[32], uint8_t iv[16], char *filename){
	char *command=calloc(64,sizeof(char));
	struct stat s;
	if(stat(filename,&s)<0) {
		fprintf(stderr, "ERROR: File %s doesn't exist\n", filename);
		return;
	}
	sprintf(command,"PUT %s %ld",filename,s.st_size);

	WORD key_schedule[60];
	BYTE enc_buf[64];
	BYTE plaintext[64];
	pad(command,strlen(command),64);
	memcpy(plaintext,command,64);

	aes_key_setup(aeskey, key_schedule, 256);
	aes_encrypt_cbc(plaintext, 64, enc_buf, key_schedule, 256, iv);
	rio_writen(clientfd,enc_buf,64);

	int localfile=open(filename,O_RDONLY,0777);
	char buf[64];
	int uploaded=0;
	int n;
	while((n=rio_readn(localfile,buf,64))>0) {
		pad(buf,n,64);
		aes_encrypt_cbc(buf,64,enc_buf,key_schedule,256,iv);
		uploaded+=n;
		rio_writen(clientfd,enc_buf,64);
	}
	close(localfile);
	printf("OK. %d bytes uploaded\n",uploaded);
}

void downloadfilelist(int clientfd, uint8_t aeskey[32], uint8_t iv[16]){
	char *command=(char *)calloc(64,sizeof(char));
	sprintf(command,"LIST");

	WORD key_schedule[60];
	BYTE enc_buf[64];
	BYTE plaintext[64];
	pad(command,strlen(command),64);
	memcpy(plaintext,command,64);

	aes_key_setup(aeskey, key_schedule, 256);
	aes_encrypt_cbc(plaintext, 64, enc_buf, key_schedule, 256, iv);
	rio_writen(clientfd,enc_buf,64);

	char numfiles[64];
	BYTE dec_buf[64];
	int realnumfiles;
	rio_readn(clientfd,numfiles,64);
	aes_decrypt_cbc(numfiles,64,dec_buf,key_schedule,256,iv);
	sscanf(dec_buf,"%d",&realnumfiles);
	/* realnumfiles now holds the number of files in server dir */

	printf("The server holds %d files\n",realnumfiles);
	if(realnumfiles==0)
		return;

	char buf[64];
	int numfilesreceived=0;
	while(numfilesreceived<realnumfiles&&rio_readn(clientfd,buf,64)>0) {
		aes_decrypt_cbc(buf,64,dec_buf,key_schedule,256,iv);
		printf("  %s\n",dec_buf);
		numfilesreceived++;
	}
	free(command);

	/* clear AES key schedule from memory */
	memset(key_schedule,0,240);
	/* explicit_bzero(key_schedule,240); */
}

void downloadfilehash(int clientfd, uint8_t aeskey[32], uint8_t iv[16], char *filename){
	char *command=(char *)calloc(64,sizeof(char));
	sprintf(command,"HASH %s",filename);

	WORD key_schedule[60];
	BYTE enc_buf[64];
	BYTE plaintext[64];
	pad(command,strlen(command),64);
	memcpy(plaintext,command,64);

	aes_key_setup(aeskey, key_schedule, 256);
	aes_encrypt_cbc(plaintext, 64, enc_buf, key_schedule, 256, iv);
	rio_writen(clientfd,enc_buf,64);

	char hash[64];
	BYTE dec_buf[64];
	uint8_t realhash[32];
	rio_readn(clientfd,hash,64);
	aes_decrypt_cbc(hash,64,dec_buf,key_schedule,256,iv);

	if(strlen(dec_buf)==0) {
		fprintf(stderr,"ERROR: Server couldn't find file %s\n",filename);
		return;
	} else {
		char message[64];
		sprintf(message,"Hash of %s is ",filename);
		printHex(dec_buf,32,message);
	}

	free(command);

	/* clear AES key schedule from memory */
	memset(key_schedule,0,240);
	/* explicit_bzero(key_schedule,240); */
}

void computefilehash(char *filename){
	struct stat s;
	if(stat(filename,&s)==0) { /* file exists, hash it */
		uint8_t realhash[32];
		SHA256_CTX ctx;
		sha256_init(&ctx);

		int localfile=open(filename,O_RDONLY,0777);
		char buf[64];
		int n;
		while((n=rio_readn(localfile,buf,64))>0) {
			sha256_update(&ctx,buf,n);
		}
		sha256_final(&ctx, realhash);

		char message[64];
		sprintf(message, "Hash of %s is ", filename);
		printHex(realhash, 32, message);
	} else {
		fprintf(stderr, "ERROR: File %s doesn't exist\n",filename);
	}
}

void checkfile(int clientfd, uint8_t aeskey[32], uint8_t iv[16], char *filename){
	printf("(Remote) ");
	fflush(stdout);
	downloadfilehash(clientfd,aeskey,iv,filename);
	printf("(Local)  ");
	fflush(stdout);
	computefilehash(filename);
}

void pad(char *buffer, int filled, int total){
	for(int i=filled; i<total; i++)
		buffer[i]='\0';
}
