CC=gcc
CFLAGS=-O2 -Wall -I. -Ilibs -Llibs

LIB=-lpthread -lcrypto

all: clean client server libraries setupdirs

client: client.c csapp.o
	$(CC) $(CFLAGS) -o file_client client.c csapp.o $(LIB)

csapp.o: csapp.c
	$(CC) $(CFLAGS) -c -o csapp.o csapp.c $(LIB)

server: server.c csapp.o
	$(CC) $(CFLAGS) -o file_server server.c csapp.o $(LIB)

libraries: libs/aes.c libs/sha256.c libs/uECC.c
	$(CC) $(CFLAGS) -c -o libs/aes.o libs/aes.c
	$(CC) $(CFLAGS) -c -o libs/sha256.o libs/sha256.c
	$(CC) $(CFLAGS) -c -o libs/uECC.o libs/uECC.c
	ar rcs libs/libcrypto.a libs/aes.o libs/sha256.o libs/uECC.o

setupdirs:
	mkdir clientdir
	mv file_client clientdir
	echo "client file" > clientdir/baz.txt
	mkdir serverdir
	mv file_server serverdir
	cp makefile serverdir
	echo "this is a test" > serverdir/foo.txt
	echo "hello world!" > serverdir/bar.txt

.PHONY:clean
clean:
	killall -q file_server | true # ignore return code of killall, otherwise make will exit early
	rm -f *.o */*.o
	rm -f file_client file_server
	rm -rf clientdir serverdir
