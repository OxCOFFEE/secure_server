// _GNU_SOURCE should be set before *any* includes.
// Alternatively, pass to compiler with -D, or enable GNU extensions
// with -std=gnu11 (or omit -std completely)
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>

int my_getrandom(void *buf, size_t buflen, unsigned int flags)
{
    return (int)syscall(SYS_getrandom, buf, buflen, flags);
}

// remove this when we have a real getrandom():
#define getrandom  my_getrandom
// and replace it with
// #include <linux/random.h>

#include "csapp.h"
#include <string.h>
#include "uECC.h"
#include "sha256.h"
#include "aes.h"
#include <strings.h>
#include <stdbool.h>

static int listenfd;
static int logfd;

void *thread(void *vargp);

void sendfile(int connfd,struct sockaddr_in addr,WORD key_schedule[60],uint8_t iv[16],char *filename);
void receivefile(int connfd,struct sockaddr_in addr,WORD key_schedule[60],uint8_t iv[16],char *filename);
void sendfilelist(int connfd,struct sockaddr_in addr,WORD key_schedule[60],uint8_t iv[16]);
void sendfilehash(int connfd, struct sockaddr_in addr, WORD key_schedule[60], uint8_t iv[16], char *filename);
void printHex(uint8_t *buf, int len, char *startmessage);
void pad(char *buffer, int filled, int total);
char* findandreplacefirst(char *buf,char tofind, char toreplace);
void printHelp(void);
void logmsg(char *message, char *arg1, char *arg2, int arg3, int mode);
void lognone(char *message);
void logs(char *message, char *arg);
void logsd(char *message, char *arg1, int arg2);
void logss(char *message, char* arg1, char *arg2);

struct threadinfo {
	int *connfdptr;
	struct sockaddr_in *addrptr;
	unsigned int *addrlenptr;
};

int main(int argc, char *argv[]){
	bool isdaemon=true;
	char *logfile="log.txt";
	int c;
	extern char *optarg;
	extern int optind;

	while((c=getopt(argc,argv,"hcl:"))!=-1) {
		switch(c) {
		case 'h':
			printHelp();/* will never return */
			break;
		case 'c':
			isdaemon=false;
			break;
		case 'l':
			logfile=optarg;
			printf("Logging on %s\n",logfile);
			break;
		default:
			exit(0);
			break;
		}
	}

	char port[6];
	if(optind!=argc-1) { /* optind is done, there is NOT 1 argument left (port), use default */
		printf("Arguments not complete. Using default settings\n");
		strcpy(port,"8080");
	} else {
		strcpy(port,argv[optind]);
	}

	logfd=isdaemon ? open(logfile,O_WRONLY|O_CREAT|O_APPEND,0777) : STDOUT_FILENO;

	printf("Server started in %s mode on port %s\n", isdaemon ? "daemon" : "console",port);
	if(isdaemon) {
		printf("Daemonizing\n");
		if(fork()!=0) /* if this is parent, kill it */
			exit(0);
		/* only child can reach this part of code */
		setsid();

		signal(SIGHUP,SIG_IGN);
		/* fork again, allow parent to die */
		if(fork()!=0) {
			_exit(0);
		}

		umask(0);

		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);
		open("/dev/null",O_RDONLY);
		open("/dev/null",O_WRONLY);
		open("/dev/null",O_RDWR);
	}

	listenfd=Open_listenfd(port);

	int *connfdp;
	pthread_t tid;
	while(1) {
		connfdp = Malloc(sizeof(int));
		struct sockaddr_in *addrp=malloc(sizeof(struct sockaddr_in));
		unsigned int *addrlenp=malloc(sizeof(unsigned int));
		*addrlenp=sizeof(*addrp);

		*connfdp = Accept(listenfd, (struct sockaddr *) addrp, addrlenp);

		struct threadinfo *ti=malloc(sizeof(struct threadinfo));
		ti->connfdptr=connfdp;
		ti->addrptr=addrp;
		ti->addrlenptr=addrlenp;

		Pthread_create(&tid, NULL, thread, ti);
	}
}

void *thread(void *vargp){
	struct threadinfo ti = *((struct threadinfo *)vargp);
	Pthread_detach(pthread_self());
	Free(vargp);

	int connfd=*(ti.connfdptr);
	struct sockaddr_in addr=*(ti.addrptr);
	unsigned int addrlen=*(ti.addrlenptr);
	free(ti.connfdptr);
	free(ti.addrptr);
	free(ti.addrlenptr);

	logsd("Connection from %s accepted on socket %d\n",inet_ntoa(addr.sin_addr),connfd);

	char buf[6];
	if(rio_readn(connfd,buf,6)!=6||strncmp(buf,"START\n",6)!=0) {
		lognone("Client didn't initialize properly! Ending connection\n");
		close(connfd);
		return NULL;
	}

	/* Begin Diffie-Hellman key exchange process */
	uint8_t private[32] = {0};
	uint8_t public[64] = {0};
	uint8_t clientpublic[64] = {0};
	uint8_t secret[32] = {0};
	const struct uECC_Curve_t *curve=uECC_secp160r1();

	uECC_make_key(public, private, curve);
	rio_writen(connfd,public,64);
	rio_readn(connfd,clientpublic,64);

	uECC_shared_secret(clientpublic, private, secret, curve);
	/* secret now holds a shared Diffie-Hellman key that must be hashed */

	/* SHA-256 hashing of secret key */
	uint8_t aeskey[32];
	SHA256_CTX ctx;
	sha256_init(&ctx);
	sha256_update(&ctx, secret, 32);
	sha256_final(&ctx, aeskey);
	/* printHex(aeskey,32,"AES key (server): "); */
	/* aeskey now holds an AES symmetric key */
	/* clear secrets from memory */
	memset(private,0,32);
    memset(secret,0,32);
	/*explicit_bzero(private,32);
	explicit_bzero(secret,32);*/

	/* generate random IV, send to client */
	uint8_t iv[16];
	getrandom(iv,16,0);
	rio_writen(connfd,iv,16);

	WORD key_schedule[60];
	BYTE dec_buf[64];
	aes_key_setup(aeskey, key_schedule, 256);

	char command[64];
	while(rio_readn(connfd,command,64)!=0) {
		aes_decrypt_cbc(command,64,dec_buf,key_schedule,256,iv);
		memcpy(command,dec_buf,64);
		if(strlen(command)<4) { /* ignore commands that are too short */
			continue;
		}
		switch(command[0]) {
		case 'G':
		case 'g':
			sendfile(connfd,addr,key_schedule,iv,command+4);
			break;
		case 'P':
		case 'p':
			receivefile(connfd,addr,key_schedule,iv,command+4);
			break;
		case 'L':
		case 'l':
			sendfilelist(connfd,addr,key_schedule,iv);
			break;
		case 'H':
		case 'h':
			sendfilehash(connfd,addr,key_schedule,iv,command+5);
			break;
		default:
			break;
		}
	}
	close(connfd);
	logsd("Connection from %s terminated on socket %d\n",inet_ntoa(addr.sin_addr),connfd);
	return NULL;
}

void sendfile(int connfd,struct sockaddr_in addr,WORD key_schedule[60],uint8_t iv[16],char *filename){
	logss("Client %s requested file %s\n",inet_ntoa(addr.sin_addr),filename);

	char *filesize=(char *)calloc(64,sizeof(char));
	struct stat s;
	if(stat(filename,&s)<0) {
		filesize[0]='0'; /*  NOT the null char, just a 0! */
	} else {
		sprintf(filesize,"%ld",s.st_size);
	}

	BYTE enc_buf[64];
	aes_encrypt_cbc(filesize,64,enc_buf,key_schedule,256,iv);
	rio_writen(connfd,enc_buf,64);

	if(s.st_size==0)
		return;

	int localfile=open(filename,O_RDONLY,0777);
	char buf[64];
	int n;
	int sent;
	while((n=rio_readn(localfile,buf,64))>0) {
		pad(buf,n,64);
		aes_encrypt_cbc(buf,64,enc_buf,key_schedule,256,iv);
		sent+=rio_writen(connfd,enc_buf,64);
	}
	close(localfile);
}

void receivefile(int connfd,struct sockaddr_in addr,WORD key_schedule[60],uint8_t iv[16],char *filename){
	char *filesize=findandreplacefirst(filename,' ','\0');
	int realfilesize;
	sscanf(filesize,"%d",&realfilesize);

	logss("Client %s requested upload of file %s\n",inet_ntoa(addr.sin_addr),filename);
	int localfile=open(filename,O_WRONLY|O_CREAT|O_TRUNC,0777);
	char buf[64];
	BYTE dec_buf[64];
	int received=0;
	while(received<realfilesize&&rio_readn(connfd,buf,64)>0) {
		aes_decrypt_cbc(buf,64,dec_buf,key_schedule,256,iv);
		received+=rio_writen(localfile,dec_buf,(realfilesize-received)>=64 ? 64 : (realfilesize-received));
	}
	close(localfile);
}

void sendfilelist(int connfd,struct sockaddr_in addr,WORD key_schedule[60],uint8_t iv[16]){
	logss("Client %s requested list of files%s\n",inet_ntoa(addr.sin_addr),"");

	char *numfiles=(char *)calloc(64,sizeof(char));
	int realnumfiles=0;

	DIR *d;
	struct dirent *dir;
	if((d=opendir("."))!=0) {
		while((dir=readdir(d))!=NULL) {
			if(dir->d_type==DT_REG)  /* skip directories */
				realnumfiles++;
		}
		closedir(d);
	}
	sprintf(numfiles,"%d",realnumfiles);

	BYTE enc_buf[64];
	aes_encrypt_cbc(numfiles,64,enc_buf,key_schedule,256,iv);
	rio_writen(connfd,enc_buf,64);

	if(realnumfiles==0) {
        free(numfiles);
        return;
    }

	char buf[64];
	if((d=opendir("."))!=0) {
		while((dir=readdir(d))!=NULL) {
			if(dir->d_type==DT_REG) {/* skip directories */
				strncpy(buf,dir->d_name,64);
				pad(buf,strlen(buf),64);
				aes_encrypt_cbc(buf,64,enc_buf,key_schedule,256,iv);
				rio_writen(connfd,enc_buf,64);
			}
		}
		closedir(d);
	}
	free(numfiles);
}

void sendfilehash(int connfd,struct sockaddr_in addr,WORD key_schedule[60],uint8_t iv[16],char *filename){
	logss("Client %s requested hash of file %s\n",inet_ntoa(addr.sin_addr),filename);

	char *hash=(char *)calloc(64,sizeof(char));
	struct stat s;
	if(stat(filename,&s)==0) {
		/* file exists, hash it and send the raw hash (encrypted) to client, NOT the ASCII version of it */
		/* (just because the ASCII version is 64 bytes, plus the terminating null it won't fit on a block) */
		uint8_t realhash[32];
		SHA256_CTX ctx;
		sha256_init(&ctx);

		int localfile=open(filename,O_RDONLY,0777);
		char buf[64];
		int n;
		while((n=rio_readn(localfile,buf,64))>0) {
			sha256_update(&ctx,buf,n);
		}
		sha256_final(&ctx, realhash);

		memcpy(hash,realhash,32);
		close(localfile);
	}
	/* else, file does not exist, just send a string full of nulls (encrypted) to client */

	BYTE enc_buf[64];
	aes_encrypt_cbc(hash,64,enc_buf,key_schedule,256,iv);
	rio_writen(connfd,enc_buf,64);

	free(hash);
}

void printHex(uint8_t *buf, int len, char *startmessage){
	printf("%s",startmessage);
	for(int i=0; i<len; i++)
		printf("%02x",buf[i]);
	printf("\n");
}

void pad(char *buffer, int filled, int total){
	for(int i=filled; i<total; i++)
		buffer[i]='\0';
}

/*
    Replace first appearance of character tofind with character toreplace on string buf.
    Return a pointer to the character just after the replaced one.
    If character tofind was not found, return a pointer to the first character of buf
 */
char* findandreplacefirst(char *buf,char tofind, char toreplace){
	for(int i=0; i<strlen(buf); i++) {
		if(buf[i]==tofind) {
			buf[i]=toreplace;
			return &(buf[i+1]);
		}
	}
	return buf;
}

void printHelp(){
	printf("Usage: file_server [options] [port]\n");
	printf("Options:\n");
	printf("    -h: help (this message)\n");
	printf("    -l <logfile>: set a custom logfile (default is ./log.txt). Only applies on daemon mode\n");
	printf("    -c: Console mode (instead of daemon)\n");
	exit(0);
}

void logmsg(char *message, char *arg1, char *arg2, int arg3, int mode){
	time_t timer;
	char buffer[26];
	struct tm* tm_info;

	time(&timer);
	tm_info = localtime(&timer);
	strftime(buffer, 26, "%b %d %Y %H:%M:%S", tm_info);
	/* buffer now holds the formatted time */

	char formatstr[256];
	strcpy(formatstr,"[");
	strcat(formatstr,buffer);
	strcat(formatstr,"] ");
	strcat(formatstr,message);

	switch(mode) {
	case 0:
		dprintf(logfd,formatstr);
	case 1:
		dprintf(logfd,formatstr,arg1);
		break;
	case 2:
		dprintf(logfd,formatstr,arg1,arg3);
		break;
	case 3:
		dprintf(logfd,formatstr,arg1,arg2);
	default:
		break;
	}
}

void lognone(char *message){
	logmsg(message,"","",0,0);
}

void logs(char *message, char *arg){
	logmsg(message,arg,"",0,1);
}

void logsd(char *message, char *arg1, int arg2){
	logmsg(message,arg1,"",arg2,2);
}

void logss(char *message, char *arg1, char *arg2){
	logmsg(message,arg1,arg2,0,3);
}
