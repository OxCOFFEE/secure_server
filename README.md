# Secure Server

This is a secure (?) C file server (plus client program).
It supports upload and download of files, all encrypted with AES-256 for (some degree of) privacy.
See [this image](process.png) for a graphical explanation (note: ~~there _may_ be a couple of errors in that image~~ there is at least one error in that image, the initialization vector is _not_ 32 bytes long, it should be 16 bytes long, see [a very reputable source](https://crypto.stackexchange.com/a/50786)).

## Collaborators
* [Ysabel Yamileth Atiencia Calle](https://bitbucket.org/yatienciac/)
* [José Reyes Ruiz](https://bitbucket.org/OxCOFFEE/)

## Compiling
`make` will compile the code and set up some directories for testing.
After compiling, there will be a folder `clientdir/` with `file_client` and file `baz.txt` on it. This will be the "client directory".
Another folder, `serverdir/`, will contain the server program `file_server` and files `makefile` (a copy of the real Makefile), `foo.txt` and `bar.txt`.

This means you can test uploads (with `baz.txt`) and downloads (`makefile`, `foo.txt` and `baz.txt`). See below for instructions on using the [client](#client-program) and [server](#server-program) programs.

For some extra granularity, use the following Make targets:
- `make server` to compile the server.
- `make client` to compile the client.
- `make libraries` to compile the libraries.
- `make setupdirs` to create simulated, isolated client and server directories to experiment with uploads and downloads.

## Server program
The server program is called like so:
```
./file_server [-h] [-c] [-l <log file>] <listening_port>
```
`-h prints a help message and exits`. `-c` runs the server in console mode (by default, the server becomes a daemon and writes to a log file, instead of STDOUT). If `-l` is passed, its arguments is the log file to use (by default, it's `log.txt`, on the same folder that the server program lives on). `-l` only has effect if `-c` is not passed (otherwise, the log is written to console instead). If the port argument is not passed, the program defaults to port 8080. The server program never exits.

## Client program
The client program is called like so:
```
./file_client [-h] <server_ip_or_name> <server_port>
```
`-h` prints a help message and exits. If 0 or 1 arguments are passed, the client program will connect by default to `localhost:8080`.
The client program displays a Telnet-like prompt, with seven commands:

### GET filename
The command `GET filename` asks the server for a file. If the file isn't available, an error message will appear and the client program
will return to the prompt. Otherwise, the file will be downloaded and saved in the same directory as `./file_client`.
### PUT filename
The command `PUT filename` uploads a file to the server. If the file doesn't exist, an error message will appear and the client program
will return to the prompt. Otherwise, the file will be uploaded and saved in the server in the same directory as `./file_server`.
### LIST
The command `LIST` asks the server for a list of files (excludes subdirectories) on its working directory. If there are no files, the server
will return 0. Otherwise, a list of files will be printed on the client. No files will be altered on either the client or the server.
### HASH filename
The command `HASH` asks the server for the SHA-256 checksum of a file. If the file isn't available, an error message will appear and the client
program will return to the prompt. Otherwise, the hash of the file will be printed on the client. No files will be altered on either the
client or the server.
### HASHLOCAL filename
The command `HASHLOCAL` computes the SHA-256 checksum of a _local_ file. If the file isn't available, an error message will appear and the client
program will return to the prompt. Otherwise, the hash of the file will be printed on the client. No files will be altered on either the
client or the server. Note that this command doesn't even connect to the server.
### CHECK filename
The command `CHECK` is a mix of `HASH` and `HASHLOCAL`. In fact, it calls the exact same functions, only with a little extra text appended.
Refer to the sections about [HASH](#hash-filename) and [HASHLOCAL](#hashlocal-filename) for more info.
It is intended to be used after a download/upload, to check if the transfer was successful. No files will be altered on either the
client or the server.
### BYE
The command `BYE` closes the connection to the server and exits the client program.


## External libraries
- AES-256 and SHA-256 implementations come from [this repo](https://github.com/B-Con/crypto-algorithms).
- Keys are generated via Elliptic Curve Diffie-Hellman key exchange (ECDH), implemented [here](https://github.com/kmackay/micro-ecc).
- Much of the heavy lifting is handled by the `csapp` library, which comes from the book [Computer Systems: A Programmer's Perspective, 3/E](http://csapp.cs.cmu.edu/).

Note that no external libraries are required. The needed code is contained in the `libs/` folder.

## Security concerns
I am MOST DEFINITELY NOT a crypto expert, which means you _shouldn't_ use _any_ of this code in anything even remotely related to sensitive data.
I am not at all sure that this code is safe (it surely isn't).
